package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Collections.reverseOrder;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<String, List<Customer>> cuisineCustomerMap = new ConcurrentHashMap<>();
    private Map<String, List<Cuisine>> customerCuisineMap = new ConcurrentHashMap<>();
    private Map<Cuisine, Long> cuisineToCustomerCounts = new ConcurrentHashMap<>();


    @Override
    public void register(final Customer customer, final Cuisine cuisine) {

        String cuisineName = cuisine.getName();
        cuisineCustomerMap.putIfAbsent(cuisineName, new LinkedList<>());
        cuisineCustomerMap.get(cuisineName).add(customer);

        customerCuisineMap.putIfAbsent(customer.getUuid(), new LinkedList<>());
        customerCuisineMap.get(customer.getUuid()).add(cuisine);

        cuisineToCustomerCounts.putIfAbsent(cuisine, 0L);
        cuisineToCustomerCounts.put(cuisine, cuisineToCustomerCounts.get(cuisine) + 1);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine == null) {
            return emptyList();
        }

        List<Customer> customers = cuisineCustomerMap.get(cuisine.getName());
        return customers != null ? customers : emptyList();
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer == null) {
            return emptyList();
        }

        List<Cuisine> cuisines = customerCuisineMap.get(customer.getUuid());
        return cuisines != null ? cuisines : emptyList();
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return cuisineToCustomerCounts.entrySet().stream()
                .sorted(reverseOrder(Map.Entry.comparingByValue()))
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
