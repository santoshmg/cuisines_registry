package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;
    private Customer customer;
    private Cuisine germanCuisine;
    private Cuisine frenchCuisine;
    private Cuisine italianCuisine;

    @Before
    public void setUp() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();

        customer = new Customer("1");

        germanCuisine = new Cuisine("german");
        frenchCuisine = new Cuisine("french");
        italianCuisine = new Cuisine("italian");
    }

    @Test
    public void cuisineCustomers_shouldReturnEmptyListForNullCuisine() {
        // given

        // when
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);

        // then
        assertThat(customers.size()).isEqualTo(0);
    }

    @Test
    public void cuisineCustomers_shouldReturnEmptyForUnknownCuisine() {
        // given

        // when
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("unknown"));

        // then
        assertThat(customers).isEmpty();
    }

    @Test
    public void customerCuisines_shouldReturnEmptyListForNullCustomer() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void customerCuisines_shouldReturnEmptyListForUnknownCustomer() {
        // given

        // when
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(customer);

        // then
        assertThat(cuisines).isEmpty();
    }

    @Test
    public void shouldRegisterCustomersForMultipleCuisines() {
        // given
        Customer customer1 = new Customer("1");
        Customer customer2 = new Customer("2");
        Customer customer3 = new Customer("3");

        // when
        cuisinesRegistry.register(new Customer("1"), frenchCuisine);
        cuisinesRegistry.register(new Customer("2"), germanCuisine);
        cuisinesRegistry.register(new Customer("3"), italianCuisine);

        // then
        List<Customer> french = cuisinesRegistry.cuisineCustomers(frenchCuisine);
        assertThat(french.size()).isEqualTo(1);
        assertThat(french.contains(customer1)).isTrue();

        List<Customer> german = cuisinesRegistry.cuisineCustomers(germanCuisine);
        assertThat(german.size()).isEqualTo(1);
        assertThat(german.contains(customer2)).isTrue();

        List<Customer> italian = cuisinesRegistry.cuisineCustomers(italianCuisine);
        assertThat(italian.size()).isEqualTo(1);
        assertThat(italian.contains(customer3)).isTrue();

    }

    @Test
    public void shouldRegisterMultipleCustomersForSameCuisine() {
        // given
        Customer customerTwo = new Customer("2");
        Customer customerThree = new Customer("3");
        Customer customerFour = new Customer("4");

        // when
        cuisinesRegistry.register(customer, germanCuisine);
        cuisinesRegistry.register(customerTwo, germanCuisine);
        cuisinesRegistry.register(customerThree, germanCuisine);
        cuisinesRegistry.register(customerFour, germanCuisine);

        // then
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(germanCuisine);
        assertThat(customers.size()).isEqualTo(4);
        assertThat(customers).contains(customer, customerTwo, customerThree, customerFour);
    }

    @Test
    public void shouldRegisterSameCustomerWithMultipleCuisine() {
        // given

        // when
        cuisinesRegistry.register(customer, germanCuisine);
        cuisinesRegistry.register(customer, frenchCuisine);
        cuisinesRegistry.register(customer, italianCuisine);

        // then
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(customer);
        assertThat(cuisines.size()).isEqualTo(3);
        assertThat(cuisines).contains(germanCuisine, italianCuisine, frenchCuisine);

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(frenchCuisine);
        assertThat(customers.size()).isEqualTo(1);
        assertThat(customers).contains(customer);
    }

    @Test
    public void topCuisines_shouldReturnListOfTopCuisines() {
        // given
        Customer customerTwo = new Customer("2");
        Customer customerThree = new Customer("3");
        Customer customerFour = new Customer("4");

        cuisinesRegistry.register(customer, germanCuisine);
        cuisinesRegistry.register(customerTwo, germanCuisine);
        cuisinesRegistry.register(customer, frenchCuisine);
        cuisinesRegistry.register(customerTwo, italianCuisine);
        cuisinesRegistry.register(customerThree, italianCuisine);
        cuisinesRegistry.register(customerFour, italianCuisine);

        // when
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);

        // then
        assertThat(cuisines).isNotNull();
        assertThat(cuisines.size()).isEqualTo(2);
        assertThat(cuisines).containsSequence(italianCuisine, germanCuisine);
        assertThat(cuisines).doesNotContain(frenchCuisine);
    }

    @Test
    public void topCuisines_shouldReturnEmptyListWhenNoUserRegisteredYet() {
        // given

        // when
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);

        // then
        assertThat(cuisines).isEmpty();
    }
}